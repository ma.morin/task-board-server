package main

import (
	"fmt"

	"gitlab.com/ma.morin/task-board-server/data"
	"gitlab.com/ma.morin/task-board-server/server"
)

func main() {
	fmt.Println("Task board server started.")
	data.Init()
	server.Start()
}
