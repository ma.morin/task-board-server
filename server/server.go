package server

import (
	"net/http"

	"github.com/gorilla/mux"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	message := "Task Board API"
	w.Write([]byte(message))
}

// Start the server
func Start() {
	router := mux.NewRouter()
	apiRouter := router.PathPrefix("/api").Subrouter()
	apiRouter.HandleFunc("", sayHello)
	http.Handle("/", router)

	if err := http.ListenAndServe(":4000", nil); err != nil {
		panic(err)
	}
}
