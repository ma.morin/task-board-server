package data

import (
	"encoding/json"
	"os"
	"strings"

	"github.com/peterbourgon/diskv"
	"gitlab.com/ma.morin/task-board-server/models"
)

var storage *diskv.Diskv

// GenerateKey generates keys from a string
func GenerateKey(str string) string {
	return strings.ToLower(
		strings.Replace(str, " ", "-", -1))
}

// initStorage initializes the data access
func initStorage() {
	flatTransform := func(s string) []string { return []string{} }

	storage = diskv.New(diskv.Options{
		BasePath:     os.Getenv("HOME") + "/.taskboard/",
		Transform:    flatTransform,
		CacheSizeMax: 1024 * 1024,
	})
}

// loadBoard loads a specified board.
func loadBoard(id string) (models.Board, error) {
	var board models.Board

	value, err := storage.Read(id)
	if err != nil {
		return board, err
	}

	if err := json.Unmarshal(value, &board); err != nil {
		return board, err
	}

	return board, nil
}

// loadBoards loads all boards.
func loadBoards() []models.Board {
	boards := []models.Board{}
	for _, key := range loadBoardKeys() {
		board, err := loadBoard(key)
		if err != nil {
			boards = append(boards, board)
		}
	}
	return boards
}

// loadBoardKeys returns keys
func loadBoardKeys() []string {
	keys := []string{}
	cancel := make(chan struct{})

	for key := range storage.Keys(cancel) {
		keys = append(keys, key)
	}
	return keys
}

// SaveBoard saves the specified board to file.
func saveBoard(board models.Board) {
	js, jsonErr := json.Marshal(board.ID)
	if jsonErr != nil {
		panic(jsonErr)
	}

	if storageErr := storage.Write(board.ID, js); storageErr != nil {
		panic(storageErr)
	}
}
