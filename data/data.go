package data

import (
	"gitlab.com/ma.morin/task-board-server/models"
)

var boards map[string]models.Board

// initMainBoard initialize the main board
func initMainBoard() {
	board, err := loadBoard("main")
	if err == nil {
		boards[board.ID] = board
	} else {
		board := models.CreateBoard("main", "Main board")
		saveBoard(board)
		boards[board.ID] = board
	}
}

// Init initializes the server data
func Init() {
	boards = make(map[string]models.Board)
	initStorage()
	initMainBoard()
}
