package models

// Board structure
type Board struct {
	ID    string
	Title string
}

// CreateBoard creates a board
func CreateBoard(id string, title string) Board {
	board := Board{
		ID:    id,
		Title: title,
	}
	return board
}
